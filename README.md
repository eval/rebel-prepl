# rebel-prepl

Connect to a socket server using rebel-readline.

[![discuss at Clojurians-Zulip](https://img.shields.io/badge/clojurians%20zulip-clojure-brightgreen.svg)](https://clojurians.zulipchat.com/#narrow/stream/151168-clojure)

## usage

Assuming a Clojure-process with a socket server:

``` shell
# using a repl as example, but any long-running process will do
$ clj -J-Dclojure.server.jvm="{:port 5555 :accept clojure.core.server/io-prepl}" -r
user=> (def a 1)
#'user/a
user=> _
```

...then start the client:

``` shell
# NOTE: use `clojure`, not `clj` to ensure rlwrap is not used
$ clojure -Sdeps '{:deps {rebel-prepl {:git/url "https://gitlab.com/eval/rebel-prepl" :sha "113bf17b9e6209369e2f541d6f27aa07ddd9d65f"}}}' -m rebel-prepl.main
# for non-default host and port ("127.0.0.1" and 5555)
$ clojure -m rebel-prepl.main --host myhost --port 5556
user=> a
1
user=> _
```

## known issues

- completion  
  Only vars in the client process are considered (i.e. Clojure functions will be completed, vars from the server-process will not).
- clojure-eval-at-point (^X^E)  
  The form is eval-ed in the client process.
- switching namespaces  
  The prompt right after switching namespaces (e.g. `(ns foo)`, `(in-ns 'foo)`) does not show the new namespace. The next prompt will however.

## license

See [LICENSE](LICENSE).

## third party licenses

This software contains code from Clojure which is distributed with the following notice:

 *   Copyright (c) Rich Hickey. All rights reserved.
 *   The use and distribution terms for this software are covered by the
 *   Eclipse Public License 1.0 (http://opensource.org/licenses/eclipse-1.0.php)
 *   which can be found in the file epl-v10.html at the root of this distribution.
 *   By using this software in any fashion, you are agreeing to be bound by
 * 	 the terms of this license.
 *   You must not remove this notice, or any other, from this software.
