(ns rebel-prepl.main
  (:require
   [clojure.core.match :refer [match]]
   [clojure.core.server]
   [clojure.main]
   [rebel-readline.clojure.line-reader]
   [rebel-readline.clojure.service.local]
   [rebel-readline.core])
  ;; for service
  (:require
   [rebel-readline.clojure.line-reader :as clj-reader]
   [rebel-readline.clojure.service.local :as local-service]
   [rebel-readline.tools :as tools])
  (:import
   [clojure.lang LineNumberingPushbackReader]
   [java.net InetAddress Socket ServerSocket SocketException]
   [java.io Reader Writer PrintWriter BufferedWriter BufferedReader InputStreamReader OutputStreamWriter]
   [java.util.concurrent.locks ReentrantLock])
  (:import (java.io StringReader)
           (clojure.lang LineNumberingPushbackReader)))

(derive :rebel-prepl.prepl/service ::clj-reader/clojure)
(derive :rebel-prepl.prepl/service ::local-service/service)


(defmethod clj-reader/-current-ns :rebel-prepl.prepl/service [{::keys [current-ns-atom] :as args}]
  #_(prn :current-ns :args args)
  (some-> current-ns-atom deref))


(defn create-service [options]
  (merge clj-reader/default-config
         (tools/user-config)
         options
         {:rebel-readline.service/type :rebel-prepl.prepl/service}))


(defmulti handle-tag (fn [{tag :tag :as resp} _] #_(prn :handle-tag resp) tag))

(defmethod handle-tag :out [{v :val} _] (print v))

(defmethod handle-tag :err [{v :val} _] (binding [*out* *err*] (print v)))

(defmethod handle-tag :ret [{:keys [ns] :as resp} {::keys [current-ns-atom]}]
  (when ns
    (reset! current-ns-atom ns))
  (let [to-print
        (match [resp]
               [{:exception true :val {:via ([err & _] :seq) :cause cause}}] (str (:message err) \newline cause)
               [{:val ([var & [var-name]] :seq)}] (str "#'" var-name)
               [{:val val}] val
               :else :unknown)]
    (println to-print)))

(defn as-stream [s]
  (-> (StringReader. s) LineNumberingPushbackReader.))

;; copied from clojure.core.server
;; (https://github.com/clojure/clojure/blob/75f8bc98b4e8d46dbaa28e54adfa24946e314bbe/src/clj/clojure/core/server.clj)
(defn- ex->data
  [ex phase]
  (assoc (Throwable->map ex) :phase phase))

;; copied from clojure.core.server
(defmacro ^:private thread
  [^String name daemon & body]
  `(doto (Thread. (fn [] ~@body) ~name)
     (.setDaemon ~daemon)
     (.start)))

;; copied from clojure.core.server
(defn- resolve-fn [valf]
  (if (symbol? valf)
    (or (resolve valf)
        (when-let [nsname (namespace valf)]
          (require (symbol nsname))
          (resolve valf))
        (throw (Exception. (str "can't resolve: " valf))))
    valf))


;; derivitive of clojure.core.server/remote-prepl
;; (https://github.com/clojure/clojure/blob/75f8bc98b4e8d46dbaa28e54adfa24946e314bbe/src/clj/clojure/core/server.clj#L295)
(defn remote-prepl
  "Variant of clojure.core.server/remote-prepl that prevents main-thread
  to hang on in-reader's eos (ie this closes the socket instead of the socket's reader)."
  [^String host port ^Reader in-reader
   out-fn & {:keys [valf readf] :or {valf read-string, readf #(read %1 false %2)}}]
  (let [valf              (resolve-fn valf)
        readf             (resolve-fn readf)
        ^long port        (if (string? port) (Integer/valueOf ^String port) port)
        socket            (Socket. host port)
        rd                (-> socket .getInputStream InputStreamReader. BufferedReader. LineNumberingPushbackReader.)
        wr                (-> socket .getOutputStream OutputStreamWriter.)
        EOF               (Object.)
        in-reader-closed? (volatile! false)]
    (thread "rebel-prepl.main/remote-prepl" true
            (try (loop []
                   (let [{:keys [tag val] :as m} (readf rd EOF)]
                     (when-not (identical? m EOF)
                       (out-fn
                        (if (#{:ret :tap} tag)
                          (try
                            (assoc m :val (valf val))
                            (catch Throwable ex
                              (assoc m :val (ex->data ex :read-eval-result)
                                     :exception true)))
                          m))
                       (recur))))
                 (catch Throwable ex
                   (when-not @in-reader-closed?
                     (throw ex)))))
    (let [buf (char-array 1024)]
      (try (loop []
             (let [n (.read in-reader buf)]
               (when-not (= n -1)
                 (.write wr buf 0 n)
                 (.flush wr)
                 (recur))))
           (finally
             (vreset! in-reader-closed? true)
             (.close socket))))))


(defn -main [& {:as args}]
  (let [port            (Integer/parseInt (get args "--port" "5555"))
        host            (get args "--host" "127.0.0.1")
        current-ns-atom (atom "user")]
    (println "Connected" :host host :port port)
    (let [service     (create-service {::current-ns-atom current-ns-atom})
          #_#_service (rebel-readline.clojure.service.local/create)]
      (rebel-readline.core/with-readline-in
        (rebel-readline.clojure.line-reader/create service)
        (remote-prepl host port *in*
                      #(handle-tag % service)
                      :valf (fn [s]
                              (binding [*default-data-reader-fn* tagged-literal]
                                (read-string s))))))))

(comment
  (require '[rebel-readline.clojure.service.simple :as simple])
  (simple/create)

  (def service (create-service {}))

  (def in-reader (as-stream "hello\nworld"))
  (def buf (char-array 1024))
  (try (loop []
         (let [n (.read in-reader buf)]
           (if-not (= n -1)
             (do (prn :processing)
              (recur))
             (prn :done))))
       (finally
         (prn :finally)
         (prn (into [] buf))))
  )
